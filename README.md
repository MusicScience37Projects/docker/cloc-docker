# cloc-docker

[![pipeline status](https://gitlab.com/MusicScience37Projects/docker/cloc-docker/badges/main/pipeline.svg)](https://gitlab.com/MusicScience37Projects/docker/cloc-docker/-/commits/main)

Docker image to use [cloc](https://github.com/AlDanial/cloc).

Current version of cloc is 1.96.

## Container Registries

You can pull automatically built images from following registries:

- [GitLab Container Registry](https://gitlab.com/MusicScience37Projects/docker/cloc-docker/container_registry)
  - latest stable image: `registry.gitlab.com/musicscience37projects/docker/cloc-docker:latest`

## Repositories

- [GitLab](https://gitlab.com/MusicScience37Projects/docker/cloc-docker):
  for development including CI

## Testing

For test of this project,
use `./tool.py test` command.
